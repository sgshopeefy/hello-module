package greetings

import (
	"errors"
	"testing"
)

func TestHello(t *testing.T) {

	t.Run("saying hello to people", func(t *testing.T) {
		got, err := Hello("Chris")
		if got != "Hello, Chris" || err != nil {
			t.Errorf("got %q want %q", got, "Hello, Chris")
		}
	})

	t.Run("throw error with 'empty name' when an empty string is supplied", func(t *testing.T) {
		_, err := Hello("")
		if err == nil || err.Error() != "empty name" {
			t.Errorf("got %q want %q", err, errors.New("empty name"))
		}
	})
}
